local sensorInfo = {
	name = "parseMap",
	desc = "Return which map positions are safe.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere

local myTeamID = Spring.GetMyTeamID()


-- @description return whether given coordinates are inside the game map
function inMap(x, z)
    return (
        x >= 0 and
        z >= 0 and
        x <= Game.mapSizeX and
        z <= Game.mapSizeZ
    )
end

-- @description return which map positions are safe
return function(stride, radius)

	-- get heightmap
	local height= {}
	for x=0, Game.mapSizeX, stride do
		height[x] = {}
		for z=0, Game.mapSizeZ, stride do
			height[x][z] = SpringGetGroundHeight(x, z)
		end
	end

	-- get IDs of my units
	local myUnit = {}
	for _, id in pairs(SpringGetTeamUnits(myTeamID)) do
		myUnit[id] = true
	end

	-- mark safe map positions
	local safe= {}
	for x=0, Game.mapSizeX, stride do
		safe[x] = {}
		for z=0, Game.mapSizeZ, stride do
			-- check in enemy units are nearby
			local onlyAliesNear = true
			for _, id in pairs(SpringGetUnitsInSphere(x, height[x][z], z, radius)) do
				if not myUnit[id] then
					onlyAliesNear = false
				end
			end

			-- check if positions and its neighbours are in a canyon
			local isCanyon = true
			for dx=-stride, stride, stride do
				for dz=-stride, stride, stride do
					local nx, nz = x + dx, z + dz
					if inMap(nx, nz) and height[nx][nz] >= 430 then
						isCanyon = false
						break
					end
				end
			end

			safe[x][z] = isCanyon or onlyAliesNear
		end
	end
	
	return { safe=safe, stride=stride }
end
