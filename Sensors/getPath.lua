local sensorInfo = {
	name = "getPath",
	desc = "Return safe path between 2 points/units.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitPosition = Spring.GetUnitPosition


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description return whether given coordinates are inside the game map
function inMap(x, z)
    return (
        x >= 0 and
        z >= 0 and
        x <= Game.mapSizeX and
        z <= Game.mapSizeZ
    )
end

-- @description return nearest point to given location which lies on parsed grid
function nearestGridPoint(location, safeMap)
    -- get nearest grid location to the top left
    local baseLoc = {
        x = safeMap.stride * math.floor(location.x / safeMap.stride),
        z = safeMap.stride * math.floor(location.z / safeMap.stride)
    }

    -- asume baseLoc is nearest safe grid position
    local gridLoc = baseLoc
    for dx=-1, 1 do
        for dz=-1, 1 do
            -- get grid coordinates
            local nx = baseLoc.x + dx * safeMap.stride
            local nz = baseLoc.z + dz * safeMap.stride
            
            local newDist = euclDist(location.x, location.z, nx, nz)
            local curDist = euclDist(location.x, location.z, gridLoc.x, gridLoc.z)

            if inMap(nx, nz) and newDist < curDist then
                gridLoc = {x=nx, z=nz}
            end
        end
    end
    return gridLoc
end

-- @description return location from given table with the smallest distance
function getMin(distMap, open)
    local minDist, minLoc = math.huge, nil

    -- other locations will be stored in a new table
    local newOpen = {}
    for _, l in ipairs(open) do
        if distMap[l.x][l.z] >= minDist then
            -- if location is not minimum add to new table
            newOpen[#newOpen + 1] = l
        else
            -- if new minimum has been found add previous to new table
            if minLoc ~= nil then
                newOpen[#newOpen + 1] = minLoc
            end

            -- store new minimum location
            minDist = distMap[l.x][l.z]
            minLoc = l
        end
    end

    return minLoc, newOpen
end

-- @description return position with given distance neighbouring given location
function getPrevPosition(thisLoc, diff, dist, distMap)
    for _, d in ipairs(diff) do
        local nx = thisLoc.x + d.x
        local nz = thisLoc.z + d.z
        
        if inMap(nx, nz) and distMap[nx][nz] == dist then
            return {x=nx, z=nz}
        end
    end
end

-- @description return path given target location and distance map
function getPath(target, diff, distMap)
    local loc, path = target, {}
    repeat
        -- add position to path
        local dist = distMap[loc.x][loc.z]
        path[dist] = loc
        
        -- get previous position
        loc = getPrevPosition(loc, diff, dist - 1, distMap)
    until (distMap[loc.x][loc.z] == 0)

    -- add starting location
    path[0] = loc
    return path
end

-- @description return path from given location to target location
function BFS(from, to, safeMap)
    -- position deltas to neighbouring locations 
    local DIFF = {
        {x=0, z= 0},
        {x=-safeMap.stride, z= 0},
        {x= safeMap.stride, z= 0},
        {x= 0, z=-safeMap.stride},
        {x= 0, z= safeMap.stride},
        {x= safeMap.stride, z= safeMap.stride},
        {x= safeMap.stride, z=-safeMap.stride},
        {x=-safeMap.stride, z= safeMap.stride},
        {x=-safeMap.stride, z=-safeMap.stride}
    }

    -- initialize distance map
    local distMap = {}
	for x=0, Game.mapSizeX, safeMap.stride do
		distMap[x] = {}
		for z=0, Game.mapSizeZ, safeMap.stride do
			distMap[x][z] = nil
		end
    end
    
    -- mark starting location
    distMap[from.x][from.z] = 0
    local min, open = nil, {from}

    -- search while there are open nodes
    while #open > 0 do
        -- get next location to expand
        min, open = getMin(distMap, open)

        -- for each neighbour
        for _, d in ipairs(DIFF) do
            local nx = min.x + d.x
            local nz = min.z + d.z

            -- if the location is safe and unexplored
            if (inMap(nx, nz) and
                distMap[nx][nz] == nil and
                safeMap.safe[nx][nz] and
                SpringGetGroundHeight(nx, nz) < 700
            ) then
                -- save distance to the location
                distMap[nx][nz] = distMap[min.x][min.z] + 1
                -- add to open locations
                open[#open + 1] = {x=nx, z=nz}
                
                -- check if we've found the target
                if nx == to.x and nz == to.z then
                    return getPath(to, DIFF, distMap)
                end
            end
        end
    end

    -- safe path doesn't exist
    return nil
end

-- @description return location of given unit if argument is not location already
function getUnitLoc(unit)
    if type(unit) == "table" then
        return unit
    else
        local x, y, z = SpringGetUnitPosition(unit)
        return { x=x, y=y, z=z }
    end
end

-- @description return safe path between 2 points/units
return function(start, target, safeMap)
 
    local startLoc  = getUnitLoc(start)
    local targetLoc = getUnitLoc(target)

    if startLoc == nil or startLoc.x == nil or targetLoc.x == nil or targetLoc == nil then
        return nil
    end

    return BFS(
        nearestGridPoint(startLoc, safeMap),
        nearestGridPoint(targetLoc, safeMap),
        safeMap
    )
end
