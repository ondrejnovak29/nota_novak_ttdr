local sensorInfo = {
	name = "getAvailableUnits",
	desc = "Return table of all units and transporting units.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetTeamUnits = Spring.GetTeamUnits

local myTeamID = Spring.GetMyTeamID()


-- @description return units from group with given DefID 
function filterUnits(group, defId)
	local u = {}
	for _, uid in ipairs(group) do
		if SpringGetUnitDefID(uid) == defId then
			u[#u + 1] = uid
		end
	end
	return u
end

-- @description return table of all units and transporting units
return function()
	local reserve = {}
	for _, uid in ipairs(SpringGetTeamUnits(myTeamID)) do
		reserve[uid] = -1
	end

	return {
		transports=filterUnits(units, 27),
		reserve=reserve,
	}
end
