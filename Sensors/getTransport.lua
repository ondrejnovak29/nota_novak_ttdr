local sensorInfo = {
	name = "getTransport",
	desc = "Return an alive transport from given group.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitIsDead = Spring.GetUnitIsDead


-- @description return an alive transport from given group
return function(transports, groupNumber, groups)

	for i=groupNumber, #transports, groups do
		local uid = transports[i]
		
		local isDead = SpringGetUnitIsDead(uid)
		isDead = isDead or isDead == nil
		
        if not isDead then
            return uid
        end
    end

    return nil
end
