local sensorInfo = {
	name = "gameShouldEnd",
	desc = "Returns true if bonus points have been reached.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns whether we should end the current mission
return function(info)
    return info.score >= info.scoreForBonus
end
