local sensorInfo = {
	name = "reserveUnit",
	desc = "Return id of the next unit that should be retrieved.",
	author = "Ondřej Novák",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitIsDead = Spring.GetUnitIsDead 
local SpringGetUnitPosition = Spring.GetUnitPosition

local REWARDS = {
	[ 34] = 10, -- Box-of-Death
	[108] =  5, --      Hatrack
	[ 38] =  2, --      Bulldog
	[127] =  1, --        Rocko
	[ 78] =  1, --       Hammer
}


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description return id of the next unit that should be retrieved
return function(transpId, safeArea)
	-- unit with the highest reward so far
    local topRew, topUid = 0, nil

	for uid, transport in pairs(bb.u.reserve) do
		-- get whether unit has been destroyed
		local unitDead = SpringGetUnitIsDead(uid)
		unitDead = unitDead or unitDead == nil

		-- get whether unit is being transported
		local noTransport = true
		if transport ~= -1 then
			noTransport = SpringGetUnitIsDead(transport)
			noTransport = noTransport or noTransport == nil
		end

		if not unitDead and noTransport then
			-- get reward for this unit
			local rew = REWARDS[SpringGetUnitDefID(uid)] or 0

			-- get distance of unit from safe area center
			local x, y, z = SpringGetUnitPosition(uid)
			local distance = euclDist(safeArea.center.x, safeArea.center.z, x, z)

			if rew > topRew and distance >= safeArea.radius - 25 then
				topRew, topUid = rew, uid
			end
		end
	end

	-- mark that this unit is being rescued
	if transpId ~= nil and topUid ~= nil then
		bb.u.reserve[transpId] = topUid
		bb.u.reserve[topUid] = transpId
	end

	return topUid
end
