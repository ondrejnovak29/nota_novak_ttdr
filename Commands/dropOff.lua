function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Orders a transport unit to unload in a given area.",
		parameterDefs = {
			{ 
				name = "transport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "safeArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitVelocity = Spring.GetUnitVelocity
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description check if there are units near given location
function noUnitNear(x, z)
	local y = SpringGetGroundHeight(x, z)
	return #SpringGetUnitsInSphere(x, y, z, 15) == 0
end

-- @description finds an empty location in a given area
function getDropPoint(center, radius)
	for i=1, 50 do
		local x = center.x + math.random(-radius, radius)
		local z = center.z + math.random(-radius, radius)
		local y = SpringGetGroundHeight(x, z)

		if euclDist(center.x, center.z, x, z) < radius and noUnitNear(x, y, z) then
			return {x, y, z}
		end
	end
end


local function ClearState(self)
	self.target = nil
	self.unloading = false
end

function Run(self, unitIds, p)
	-- bottom right corner of safe area
	local botRight = {
		x=p.safeArea.center.x + p.safeArea.radius,
		z=p.safeArea.center.z + p.safeArea.radius
	}
	local x, y, z = SpringGetUnitPosition(p.transport)

	-- if unit is not moving and isn't unloading
	local _, _, _, vel = SpringGetUnitVelocity(p.transport)
	if vel < 0.1 and not self.unloading then
		-- if initializing move or are is blocked
		if self.target == nil or not noUnitNear(x, z) then
			-- get new drop location
			self.target = getDropPoint(p.safeArea.center, p.safeArea.radius - 15)
			
			-- no empty location found
			if self.target == nil then
				return FAILURE
			end

			-- move to location
			SpringGiveOrderToUnit(
				p.transport,
				CMD.MOVE,
				self.target,
				{}
			)
		else
			-- unload unit at transport's location
			self.unloading = true
			SpringGiveOrderToUnit(
				p.transport,
				CMD.UNLOAD_UNIT,
				{x, y, z},
				{}
			)
			
			-- move away (so the transporter doesn't land on the unit)
			SpringGiveOrderToUnit(
				p.transport,
				CMD.MOVE,
				{botRight.x, SpringGetGroundHeight(botRight.x, botRight.z), botRight.z},
				{"shift"}
			)
		end
	end

	-- check if unloading sequence is finished
	if euclDist(botRight.x, botRight.z, x, z) < 200 then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
