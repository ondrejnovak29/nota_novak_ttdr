function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Picks up a unit using given transport.",
		parameterDefs = {
			{ 
				name = "transport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting


-- @description return `table`'s values in a new table where they function as keys
function valueSet(table)
	local set = {}
	for _, v in ipairs(table) do set[v] = true end
	return set
end

local function ClearState(self)
	self.pickingUID = nil
end

function Run(self, unitIds, p)
	-- initialize picking of unit
    if self.pickingUID == nil then
        self.pickingUID = p.unit
        SpringGiveOrderToUnit(
            p.transport,
            CMD.LOAD_UNITS,
            {self.pickingUID},
            {}
        )
    end

	-- check if unit has been picked
	local transporting = SpringGetUnitIsTransporting(p.transport)
	if valueSet(transporting)[self.pickingUID] then
		return SUCCESS
    else
        return RUNNING
    end
end

function Reset(self)
	ClearState(self)
end