function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Commands a unit to move to a sequence of locations.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitCommands = Spring.GetUnitCommands


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

local function ClearState(self)
	self.target = nil
end

function Run(self, unitIds, p)
	-- fail if no unit or path was specified
	if self.target == nil and p.path == nil then
		return FAILURE
	end

	local cmdQueue = SpringGetUnitCommands(p.unit)
	if cmdQueue == nil then
		return FAILURE
	end

	-- initialize move
	if self.target == nil or #cmdQueue == 0 then
		for _, loc in ipairs(p.path) do
			-- set next location
            self.target = {
                loc.x,
                SpringGetGroundHeight(loc.x, loc.z),
                loc.z
            }
	
			-- append move command
            SpringGiveOrderToUnit(
                p.unit,
                CMD.MOVE,
                self.target,
                {"shift"}
            )
        end
    end

	-- check if target has been reached
	local x, _, z = SpringGetUnitPosition(p.unit)
	if euclDist(self.target[1], self.target[3], x, z) < 300 then
        return SUCCESS
    else
	    return RUNNING
    end
end

function Reset(self)
	ClearState(self)
end
